FROM alpine:latest

ENV KUBECTL_URL=https://storage.googleapis.com/kubernetes-release/release/v1.17.2/bin/linux/amd64/kubectl

ENV HELM_URL=https://get.helm.sh/helm-v3.0.3-linux-amd64.tar.gz

RUN wget ${KUBECTL_URL} -O /usr/local/bin/kubectl && chmod +x /usr/local/bin/kubectl && \
    wget ${HELM_URL} && tar -xf helm-*-linux-amd64.tar.gz && \
    mv linux-amd64/helm  /usr/local/bin/helm && rm -r helm-*-linux-amd64.tar.gz linux-amd64

ENTRYPOINT ["/bin/sh", "-c"]
